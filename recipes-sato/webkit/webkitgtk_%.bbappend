FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI += "file://minibrowser.desktop"

do_install:append() {
    install -m 0644 -D ${UNPACKDIR}/minibrowser.desktop ${D}${datadir}/applications/minibrowser.desktop
}
